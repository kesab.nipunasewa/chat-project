import 'package:flutter/cupertino.dart';

import 'main_common.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(
      DevicePreview(enabled: true, builder: (context) => const Application()));
}
