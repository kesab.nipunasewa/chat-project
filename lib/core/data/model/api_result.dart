
class ApiResponse<T>  {
  ApiResponse({this.error, this.data});

  T? data;

  T? error;

  bool get hasData => data != null;

  bool get hasError => error != null;

  bool get isInitial => !hasData && !hasError;

}

// class DataResponse<T> {
//   Status status;
//   T? data; //dynamic
//   String? loadingMessage;
//   dynamic error;
//   DataResponse.init() : status = Status.init;
//
//   DataResponse.loading({this.loadingMessage}) : status = Status.loading;
//
//   DataResponse.success(this.data) : status = Status.success;
//
//   DataResponse.error(this.error) : status = Status.error;
//
//
//   @override
//   String toString() {
//     return "Status : $status \n Message : $loadingMessage \n Data : $data";
//   }
// }
//
// enum Status {
//   init,
//   loading,
//   success,
//   error,
// }