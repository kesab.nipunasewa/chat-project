import 'package:flutter/material.dart';

const kPink50 = Color(0xFFFEEAE6);
const kBlue100 = Color(0xff22478d);
const kBlue200 = Color(0xff1c366b);
const kPink300 = Color(0xFFFBB8AC);
const kPink400 = Color(0xFFEAA4A4);
//Greater the number darker is the color
const kBrown900 = Color(0xFF211519);
const kBrown600 = Color(0xFF7D4F52);

const kErrorRed = Color(0xFFC5032B);
const kGrey100 = Color(0xFFB8B4B4);
const kGrey200 = Color(0xFF938F8F);

const teal =Color(0xff088f8f);

const kSurfaceWhite = Color(0xFFFFFBFA);
const kBackgroundWhite = Colors.white;
