import 'package:flutter/material.dart';

import 'app_colors.dart';

final ThemeData kAppTheme = _buildTheme();

IconThemeData _customIconTheme(IconThemeData original) {
  return original.copyWith(color: kBrown900);
}

AppBarTheme _customAppbarIconTheme(AppBarTheme original) {
  return original.copyWith(
      elevation: 0,
      foregroundColor: Colors.white,
      iconTheme: original.iconTheme?.copyWith(
        color: Colors.white,
      ));
}

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: kAppColorScheme,
    // accentColor: kShrineBrown900,
    primaryColor: kBlue200,

    // buttonColor: kPink100,
    scaffoldBackgroundColor: kBackgroundWhite,
    cardColor: kBackgroundWhite,
    errorColor: kErrorRed,
    buttonTheme: const ButtonThemeData(
      colorScheme: kAppColorScheme,
      textTheme: ButtonTextTheme.normal,
    ),
    appBarTheme: _customAppbarIconTheme(base.appBarTheme),
    // appBarTheme: AppBarTheme(),
    primaryIconTheme: _customIconTheme(base.iconTheme),
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      fillColor: Colors.white,
      filled: true,
      isDense: true,
      hintStyle: base.textTheme.caption?.copyWith(
        fontWeight: FontWeight.w400,
        fontSize: 12.0,
      ),
    ),
    textTheme: _buildTextTheme(base.textTheme),
    primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
    iconTheme: _customIconTheme(base.iconTheme),
  );
}

TextTheme _buildTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline6: base.headline6?.copyWith(fontWeight: FontWeight.w500),
        bodyText1: base.bodyText1?.copyWith(fontSize: 18.0),
        caption: base.caption?.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 12.0,
        ),
        bodyText2: base.bodyText2?.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 14.0,
        ),
        button: base.button?.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 14.0,
        ),
      )
      .apply(
        fontFamily: 'Poppins',
        displayColor: kBrown900,
        bodyColor: kBrown900,
      );
}

const ColorScheme kAppColorScheme = ColorScheme(
  primary: kBlue200,
  secondary: kBrown900,
  surface: kSurfaceWhite,
  background: kBackgroundWhite,
  error: kErrorRed,
  onPrimary: kBrown900,
  onSecondary: kBrown900,
  onSurface: kBrown900,
  onBackground: kBrown900,
  onError: kSurfaceWhite,
  brightness: Brightness.light,
);
