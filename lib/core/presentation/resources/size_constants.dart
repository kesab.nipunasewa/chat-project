import 'package:flutter/material.dart';

class SC {
  ///[edgeSpacing] is 15
  static const edgeSpacing = 15.0;
  static const xxlH = 25.0;
  static const xlH = 20.0;
  static const lH = 15.0;
  static const mH = 10.0;
  static const sH = 5.0;
}

///Use [SBC] to add [SizedBox] of various sizes
class SBC {
  static const xxlH = SizedBox(height: 25);
  static const xlH = SizedBox(height: 20);
  static const lH = SizedBox(height: 15);
  static const mH = SizedBox(height: 10);
  static const sH = SizedBox(height: 5);
}
