import 'package:get/get.dart';

import '../../../features/auth/presentation/login_page.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const initial = Routes.login;

  static final routes = [
    GetPage(
      name: _Paths.login,
      page: () => LoginPage(),
    ),
  ];
}
