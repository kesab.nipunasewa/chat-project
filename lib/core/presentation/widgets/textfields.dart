import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../resources/app_colors.dart';

class PrimaryFormField extends HookWidget {
  final String? label;
  final String? hintTxt;
  final Widget? hintIcon;
  final bool? isFilled;
  final bool isRequired;

  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final Function(String) onSaved;
  final Widget? prefixIcon;
  final double? labelHeight;
  final bool? isPassword;
  final TextInputType? keyboardType;

  final int maxLines;
  final int? minLines;

  final String? initialValue;

  const PrimaryFormField({
    Key? key,
    this.hintTxt,
    this.initialValue,
    this.hintIcon,
    this.label,
    this.isRequired = false,
    this.validator,
    this.onChanged,
    this.maxLines = 1,
    this.minLines,
    required this.onSaved,
    this.prefixIcon,
    this.labelHeight,
    this.isPassword = false,
    this.isFilled = false,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isPasswordVisible = useState(isPassword!);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (label != null)
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (prefixIcon != null)
                Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: prefixIcon!),
              RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: label!,
                      style: Theme.of(context).textTheme.bodyText2),
                  if (isRequired)
                    TextSpan(
                        text: " *",
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            ?.copyWith(color: Colors.red)),
                ]),
              ),
            ],
          ),
        SizedBox(
          height: labelHeight ?? 10,
        ),
        TextFormField(
          minLines: minLines,
          maxLines: maxLines,
          initialValue: initialValue,
          keyboardType: keyboardType,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: validator,
          onSaved: (value) {
            onSaved(value!);
          },
          onChanged: onChanged,
          obscureText: isPasswordVisible.value,
          decoration: InputDecoration(
            errorMaxLines: 2,
            prefixIcon: hintIcon,
            suffixIcon: isPassword!
                ? GestureDetector(
                    onTap: () {
                      isPasswordVisible.value = !isPasswordVisible.value;
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                      child: Icon(isPasswordVisible.value
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ))
                : const SizedBox.shrink(),
            hintText: hintTxt,
            hintStyle: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: kGrey200),
            filled: isFilled!,
            isDense: true,
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            fillColor: Colors.white70,
            errorBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: kErrorRed),
            ),
            focusedErrorBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: kBrown900),
            ),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: kBrown900),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: kBlue200),
            ),
          ),
        ),
      ],
    );
  }
}

class SecondaryFormField extends HookWidget {
  final String? label;
  final String? hintTxt;
  final Widget? hintIcon;
  final bool? isFilled;
  final bool isRequired;

  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final Function(String) onSaved;
  final Widget? prefixIcon;
  final double? labelHeight;
  final bool? isPassword;
  final TextInputType? keyboardType;

  final int maxLines;
  final int? minLines;

  final String? initialValue;

  const SecondaryFormField({
    Key? key,
    this.hintTxt,
    this.initialValue,
    this.hintIcon,
    this.label,
    this.isRequired = false,
    this.validator,
    this.onChanged,
    this.maxLines = 1,
    this.minLines,
    required this.onSaved,
    this.prefixIcon,
    this.labelHeight,
    this.isPassword = false,
    this.isFilled = false,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isPasswordVisible = useState(isPassword!);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (label != null)
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (prefixIcon != null)
                Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: prefixIcon!),
              RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: label!,
                      style: Theme.of(context).textTheme.bodyText2?.copyWith(color: kGrey200)),
                  if (isRequired)
                    TextSpan(
                        text: " *",
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            ?.copyWith(color: Colors.red)),
                ]),
              ),
            ],
          ),
        SizedBox(
          height: labelHeight ?? 0,
        ),
        TextFormField(
          minLines: minLines,
          maxLines: maxLines,
          initialValue: initialValue,
          keyboardType: keyboardType,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: validator,
          onSaved: (value) {
            onSaved(value!);
          },
          onChanged: onChanged,
          obscureText: isPasswordVisible.value,
          decoration: InputDecoration(
            errorMaxLines: 2,
            prefixIcon: hintIcon,
            suffixIcon: isPassword!
                ? GestureDetector(
                    onTap: () {
                      isPasswordVisible.value = !isPasswordVisible.value;
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                      child: Icon(isPasswordVisible.value
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ))
                : const SizedBox.shrink(),
            hintText: hintTxt,
            hintStyle: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: kGrey200),
            filled: isFilled!,
            isDense: true,

            fillColor: Colors.white70,
            errorBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: kErrorRed),
            ),
            focusedErrorBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: kBrown900),
            ),
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: kGrey100),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Theme.of(context).primaryColor,width: 1.2),
            ),
          ),
        ),
      ],
    );
  }
}
