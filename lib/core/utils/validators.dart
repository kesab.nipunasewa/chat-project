class Validators {
  static String? isNotEmpty(String? value) {
    return value != null && value.isNotEmpty ? null : "This field is required";
  }
}
